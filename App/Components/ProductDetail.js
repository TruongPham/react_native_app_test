import React, { Component } from 'react'
import { FlatList, Image, View, Dimensions } from 'react-native'
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Spinner } from 'native-base'

export default class ProductDetail extends Component {
  render() {
    return (
      <Content>
          <Card style={{flex: 0, minHeight: Dimensions.get("window").height}}>
            <CardItem>
              <Left>
                <Body>
                  <Text>Product: iPhone8</Text>
                  <Text note>April 15, 2016</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Image source={{uri: 'https://cdn.mediamart.vn/Product/internet-tivi-sony-43-inch-43w750e-full-hd-mxr-200hz-29VXxW.png'}} style={{height: 200, width: 200, flex: 1}}/>
                <Text>
                  iPhone 8 Plus có "số đo ba vòng" 158,4 x 78,1 x 7,6 mm và có trọng lượng 202 gram. Cùng lúc, con số này trên điện thoại của Samsung là 162,5 x 74,8 x 8,6 mm và 195 gram.
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="logo-usd" />
                  <Text>1,926</Text>
                </Button>
              </Left>
            </CardItem>
          </Card>
        </Content>
    )
  }
}