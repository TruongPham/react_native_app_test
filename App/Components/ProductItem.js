import React, { Component } from 'react'
import { Image, View } from 'react-native'
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base'
export default class ProductItem extends Component {
  render() {
    return (
      <View style={{paddingLeft: 5, paddingRight: 5}}>
          <Card>
            <CardItem>
              <Left>
                <Body>
                  <Text>iPhoneX</Text>
                  <Text note>21-10-2017</Text>
                </Body>
              </Left>
              <Right>
                <Text>100.000 $</Text>
              </Right>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://cdn.mediamart.vn/Product/internet-tivi-sony-43-inch-43w750e-full-hd-mxr-200hz-29VXxW.png'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="md-brush" />
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="trash" />
                </Button>
              </Body>
              <Right>
                <Button transparent>
                  <Text>Detail</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
        </View>
    )
  }
}