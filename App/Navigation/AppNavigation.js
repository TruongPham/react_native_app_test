import { StackNavigator } from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import MainScreen from '../Containers/MainScreen'
import ProductDetailScreen from '../Containers/ProductDetailScreen'
import ProductAddScreen from '../Containers/ProductAddScreen'
import LoginScreen from '../Containers/LoginScreen'


import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  LaunchScreen: { screen: LaunchScreen },
  MainScreen: { screen: MainScreen},
  ProductDetailScreen: { screen: ProductDetailScreen},
  ProductAddScreen: { screen: ProductAddScreen},
  LoginScreen: { screen: LoginScreen }
}, {
  // Default config for all screens
  initialRouteName: 'LoginScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
