import React, { Component } from 'react'
import { ScrollView, Image, Dimensions, View, TextInput } from 'react-native'
import { Container, Button, Content, Form, Item, Input, Card, Text, Label } from 'native-base'

import { Images } from '../Themes'

// Styles
import styles from './Styles/LoginScreenStyles'

export default class LoginScreen extends Component {
  static navigationOptions = {
    title: 'Login',
  };

  constructor(props) {
    super(props)
    this.state = {
      fetching: false
    };
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <Card>
            <Content style={{flex: 0, minHeight: Dimensions.get("window").height, backgroundColor: '#fff'}}>
              <Form>
                <Item floatingLabel>
                  <Label>Email</Label>
                  <Input />
                </Item>
                <Item floatingLabel>
                  <Label>Password</Label>
                  <Input />
                </Item>
              </Form>
              <Button block style={{marginTop: 20}}>
                <Text>Login</Text>
              </Button>
          </Content>
          </Card>
        </ScrollView>
      </View>
    )
  }
}
  