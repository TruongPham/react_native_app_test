import React, { Component } from 'react'
import { ScrollView, Text, Image, View } from 'react-native'
import { Container, Header, Content, Spinner } from 'native-base'
import DevscreensButton from '../../ignite/DevScreens/DevscreensButton.js'

import { Images } from '../Themes'

// Styles
import styles from './Styles/LaunchScreenStyles'

export default class LaunchScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.logo} style={styles.logo} />
          </View>

          <View style={styles.section} >
          </View>
          <Content>
            <Spinner />
          </Content>
         <DevscreensButton />
        </ScrollView>
      </View>
    )
  }
}
