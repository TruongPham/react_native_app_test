import React, { Component } from 'react'
import { ScrollView, Image, Dimensions, View, TextInput } from 'react-native'
import { Container, Button, Content, Form, Item, Input, Card, Text, Label } from 'native-base'

import { Images } from '../Themes'

// Styles
import styles from './Styles/ProductAddScreenStyles'

export default class ProductAddScreen extends Component {
  static navigationOptions = {
    title: 'Create new Product',
  };

  constructor(props) {
    super(props)
    this.state = {
      fetching: false
    };
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <Card>
            <Content style={{flex: 0, minHeight: Dimensions.get("window").height, backgroundColor: '#fff'}}>
              <Form>
                <Item floatingLabel>
                  <Label>Name</Label>
                  <Input />
                </Item>
                <Item floatingLabel>
                  <Label>Price</Label>
                  <Input />
                </Item>
                <Item floatingLabel last>
                  <Label>Description</Label>
                  <Input 
                      multiline={true}
                      numberOfLines={7}
                  />
                </Item>
              </Form>
              <Button block style={{marginTop: 20}}>
                <Text>Submit</Text>
              </Button>
          </Content>
          </Card>
        </ScrollView>
      </View>
    )
  }
}
  