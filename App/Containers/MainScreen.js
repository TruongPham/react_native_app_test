import React, { Component } from 'react'
import { ScrollView, Text, Image, View, FlatList } from 'react-native'
import { Container, Header, Content, Spinner, List } from 'native-base'

import ProductItem from '../Components/ProductItem'

import { Images } from '../Themes'

// Styles
import styles from './Styles/MainScreenStyles'

export default class MainScreen extends Component {
  static navigationOptions = {
    title: 'Welcome',
  };

  constructor(props) {
    super(props)
    this.state = {
      products : [{key: 1}, {key: 2}, {key: 3}, {key: 4}, {key: 5}, {key: 6}, {key: 7}, {key: 8}, {key: 9}],
      fetching: false
    };
  }

  onRefresh = () => {
    this.setState({fetching: true})
    console.log('refreshing ===================')
  }

  loadMore = () => {
    console.log('load more ===================')
  }


  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <FlatList 
          data= {this.state.products}
          renderItem= {(item) => <ProductItem />}
          refreshing={this.state.fetching}
          onRefresh={this.onRefresh}
          refreshControl={<Spinner />}
          onEndReachedThreshold={1}
          onEndReached={this.loadMore}
        />
      </View>
    )
  }
}
  