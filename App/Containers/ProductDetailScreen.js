import React, { Component } from 'react'
import { FlatList, Image, View, Dimensions } from 'react-native'
import { Spinner } from 'native-base'

import ProductDetail from '../Components/ProductDetail'

import { Images } from '../Themes'

// Styles
import styles from './Styles/ProductDetailScreenStyles'

export default class ProductDetailScreen extends Component {
  static navigationOptions = {
    title: 'iPhone8',
  };

  constructor(props) {
    super(props)
    this.state = {
      fetching: false
    };
  }

  onRefresh = () => {
    this.setState({fetching: true})
    console.log('refreshing ===================')
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <FlatList style={styles.container}
          data= {[{key: 1}]}
          renderItem={(item) => <ProductDetail />}
          onRefresh={this.onRefresh}
          refreshing={this.state.fetching}
          refreshControl={<Spinner />}
        />
      </View>
    )
  }
}
  